const BaseURL = 'https://cdn2.yedi.com.ng/podcasts/';
// const BaseURL = 'http://127.0.0.1:8000/';

const headers =   {
  Accept: 'application/json',
  'Content-Type': 'application/json',
}

const postReq = async (url, body) => {
  body = JSON.stringify(body);
  const rawResponse = await fetch(BaseURL + url, {
    method: "POST",
    headers,
    body,
  });
  // const content = await rawResponse.json();
  return { rawResponse };
};

const getReq = async url => {
  const rawResponse = await fetch(BaseURL + url, { headers });
  const content = await rawResponse.json();
  return { rawResponse, content };
};

export const endpoints = {

  getAllScholars: async () => {
    try {
      const response = await getReq('scholars');
      return response;
    } 
    catch (error) {
      return error
    }
    
  },
  


}