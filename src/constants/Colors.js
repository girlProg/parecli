const tintColor = '#305F72';

export default {
  futura : 'futura',
  futuraMedium : 'futura-medium',
  futuraBold: 'futura-bold',
  defaultColor : '#3e6061',
  tintColor,
  tabIconDefault: '#ccc',
  tabIconSelected: tintColor,
  tabBar: '#FFEBCF',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#F18C8E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',
};
