import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View, Dimensions } from "react-native";
import TrackPlayer, { usePlaybackState } from "react-native-track-player";

import Player from "../components/Player";
import playlistData from "../data/playlist.json";
import localTrack from "../resources/pure.m4a";
const {width, height} = Dimensions.get('window')

export default function PlaylistScreen({route, navigation}) {
  const playbackState = usePlaybackState();
  const [allAudio, setAllAudio] = useState([])
  const { scholar } = route.params;
  useEffect(() => {
    setup(scholar.audiofiles);
  }, []);
  var auds = []

  
  async function setup(audiofiles) {
    try {
      await TrackPlayer.setupPlayer({});
      await TrackPlayer.updateOptions({
      stopWithApp: true,
      capabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
        TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
        TrackPlayer.CAPABILITY_STOP
      ],
      compactCapabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE
      ]
    });
    audiofiles.forEach(file => {
      let currTr = {
        "id": file.id,
        "url": file.file? file.file : file.external_url,
        "title": file.name,
        "artist": scholar.name,
        "artwork": "https://picsum.photos/200",
      }
      auds.push(currTr)
    });
    } catch (error) {
      console.log(error)
    }
    
  }

  async function generateSongs(audios) {
    'https://source.unsplash.com/1600x900/?nature,water'


  }
  async function togglePlayback() {
    const currentTrack = await TrackPlayer.getCurrentTrack();
    if (currentTrack == null) {
      await TrackPlayer.reset();
      await TrackPlayer.add(auds);
      await TrackPlayer.play();
    } else {
      if (playbackState === TrackPlayer.STATE_PAUSED) {
        await TrackPlayer.play();
      } else {
        await TrackPlayer.pause();
      }
    }
  }

  return (
    <View style={styles.container}>
      <View style={styles.bottomCard}>
            <Text style={styles.studentName}>{scholar.name}</Text>
            {/* <Text style={styles.secondaryText}>Primary 2A</Text>
            <Text style={styles.secondaryText}>5 Years Old</Text> */}
        </View>
      <Player
        onNext20={onNext20}
        onPrevious20={onPrevious20}
        onNext={skipToNext}
        style={styles.player}
        onPrevious={skipToPrevious}
        onTogglePlayback={togglePlayback}
      />
      <Text style={styles.state}>{getStateName(playbackState)}</Text>
    </View>
  );
}

PlaylistScreen.navigationOptions = {
  title: "Playlist Example"
};

function getStateName(state) {
  switch (state) {
    case TrackPlayer.STATE_NONE:
      return "None";
    case TrackPlayer.STATE_PLAYING:
      return "Playing";
    case TrackPlayer.STATE_PAUSED:
      return "Paused";
    case TrackPlayer.STATE_STOPPED:
      return "Stopped";
    case TrackPlayer.STATE_BUFFERING:
      return "Buffering";
  }
}

async function onNext20() {
  try {
    let position = await TrackPlayer.getPosition();
    await TrackPlayer.seekTo(position+30);
  } catch (_) {}
}

async function onPrevious20() {
  try {
    let position = await TrackPlayer.getPosition();
    await TrackPlayer.seekTo(position-30);
  } catch (_) {}
}

async function skipToNext() {
  try {
    await TrackPlayer.skipToNext();
  } catch (_) {}
}

async function skipToPrevious() {
  try {
    await TrackPlayer.skipToPrevious();
  } catch (_) {}
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  description: {
    width: "80%",
    marginTop: 20,
    textAlign: "center"
  },
  player: {
    marginTop: 40
  },
  state: {
    marginTop: 20
  },
  bottomCard:{
    marginTop: 20,
    height: 100,
    width: width/100 * 45,
    backgroundColor: '#3e6061',
    borderRadius: 20,
    // borderBottomRightRadius: 20,
    alignSelf: "center",
    justifyContent: 'center',
    shadowColor: "#adaaaa",
    shadowOpacity: 0.24,
    shadowRadius: 20,
    marginRight: 20,
    alignContent: 'stretch'
  },
  studentName: {
    marginLeft: 10,
    fontFamily: 'futura-bold',
    // fontSize: width/20,
    // width: width/100 *40,
    color: 'white'
  },
});
