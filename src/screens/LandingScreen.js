import React, {useState, useEffect} from "react";
import { StyleSheet, Text, View, TouchableOpacity, FlatList } from "react-native";
import ListItem from '../components/ListItem';
import { endpoints } from '../api/podcasts_api';


const LandingScreen = ({navigation}) => {
  const [scholars, setScholars ] = useState([])
  useEffect(()=>{get_scholars()}, [])


  const get_scholars = async () => {
      let raw_scholars = await endpoints.getAllScholars()
      const { rawResponse, content } = raw_scholars
      setScholars(content.results)
      // alert(raw_scholars)
  }

  // static navigationOptions = {
  //   title: "React Native Track Player"
  // };
    return (
      <View style={styles.container}>

        <FlatList data={scholars} keyExtractor={item => item.id.toString()} 
            renderItem={ ({ item })  =>  <ListItem image={{uri: item.image}} navigation={navigation} name={item.name} scholar={item} /> 
                    

            

         } />

        {/* <TouchableOpacity onPress={()=>navigation.navigate('Playlist')} activeOpacity={0.8} >
            <ListItem image={require('../resources/pantami1.png')} navigation={navigation} name={'Dr Isa Ali Pantami'} /> 
          </TouchableOpacity> 
        <TouchableOpacity onPress={()=>navigation.navigate('Playlist')} activeOpacity={0.8} >
            <ListItem image={require('../resources/gurumtum1.png')} navigation={navigation} name={'Ibrahim Tijjani Gurumtum'}/> 
          </TouchableOpacity> 
        <TouchableOpacity onPress={()=>navigation.navigate('Playlist')} activeOpacity={0.8} >
            <ListItem image={require('../resources/kabirugombe.png')} navigation={navigation} name={'Kabiru Gombe'}/> 
          </TouchableOpacity> 
        <TouchableOpacity onPress={()=>navigation.navigate('Playlist')} activeOpacity={0.8} >
            <ListItem image={require('../resources/anon.png')} navigation={navigation} name={'Yusuf Mohammed'}/> 
          </TouchableOpacity>  */}
          
        
        <TouchableOpacity onPress={() => this.navigateTo("Playlist")}>
          <Text>{scholars.length}</Text>
          <Text>Powered by YediTech</Text>
        </TouchableOpacity>
      </View>
    );
  
}

export default LandingScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  header: {
    fontSize: 20,
    marginTop: 20,
    marginBottom: 10,
    fontWeight: "bold",
    textAlign: "center"
  }
});
