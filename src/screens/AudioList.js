import React, {useState, useEffect} from "react";
import { StyleSheet, Text, View, TouchableOpacity, FlatList } from "react-native";
import AudioListItem from '../components/AudioListItem';


const AudioList = ({route, navigation }) => {
    const {scholar} = route.params
  // static navigationOptions = {
  //   title: "React Native Track Player"
  // };
    return (
      <View style={styles.container}>

        <FlatList data={route.params.scholar && route.params.scholar.audiofiles} keyExtractor={item => item.id?.toString()} 
            renderItem={ ({ item })  => <AudioListItem  navigation={navigation} item={item} scholar={route.params.scholar} /> 
        } />

        <TouchableOpacity onPress={() => this.navigateTo("Playlist")}>
          <Text>Playlist Example</Text>
        </TouchableOpacity>
      </View>
    );
  
}

export default AudioList

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  header: {
    fontSize: 20,
    marginTop: 20,
    marginBottom: 10,
    fontWeight: "bold",
    textAlign: "center"
  }
});
