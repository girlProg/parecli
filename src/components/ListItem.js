import React from 'react';
import { Image, StyleSheet, Text, View, Dimensions, TouchableOpacity } from 'react-native';
import Colors from '../constants/Colors';

const {width, height} = Dimensions.get('window')

const ListItem = ({name, image, navigation, scholar}) => {


  return (
    <TouchableOpacity onPress={()=>navigation.navigate('AudioList', {scholar: scholar} )} activeOpacity={0.8} >
    <View style={styles.container}>
        <View style={styles.topCard}>
            <Image style={styles.image} source={image} />
        </View>
        <View style={styles.bottomCard}>
            <Text style={styles.studentName}>{name}</Text>
            {/* <Text style={styles.secondaryText}>Primary 2A</Text>
            <Text style={styles.secondaryText}>5 Years Old</Text> */}
        </View>
    </View>
    </TouchableOpacity> 
  );
}

export default ListItem

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    marginTop: 40,
    flexDirection: 'row',
    shadowColor: "#d0d5db",
    shadowOpacity: 0.24,
    shadowRadius: 20,
    marginBottom: 20,
    // backgroundColor: '#f6f6f6',
    // alignContent: 'center',
    // justifyContent: 'center',
  },
  bottomCard:{
    // marginTop: 200,
    height: 100,
    width: width/100 * 45,
    backgroundColor: Colors.defaultColor,
    borderTopRightRadius: 20,
    borderBottomRightRadius: 20,
    alignSelf: "center",
    justifyContent: 'center',
    shadowColor: "#adaaaa",
    shadowOpacity: 0.24,
    shadowRadius: 20,
    marginRight: 20,
    alignContent: 'stretch'
  },
  image: {
      marginBottom: 20,
    height: 150,
    width: width/100 *45,
    borderRadius: 20,
    alignSelf: "center",
    justifyContent: 'center',
    shadowColor: "#d0d5db",
    shadowOpacity: 0.24,
    shadowRadius: 20,
  },
  topCard: {
      
      marginLeft: 20,
    // marginTop: 200,
    height: 130,
    width: width/100 *45,
    backgroundColor: '#c4d1d1',
    borderRadius: 20,
    alignSelf: "center",
    justifyContent: 'center',
    shadowColor: "#d0d5db",
    shadowOpacity: 0.24,
    shadowRadius: 20,
  },
  studentName: {
    marginLeft: 10,
    fontFamily: 'futura-bold',
    fontSize: width/20,
    width: width/100 *40,
    color: 'white'
  },
  secondaryText: {
    marginTop: 10,
    marginLeft: 10,
    fontFamily: 'futura',
    fontSize: width/30,
    color: '#fff'
  }
});
